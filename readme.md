## Boonyapon's Comment

I can't create new branch or pull request . My access level on repositories is Can Read. 

1. InquiryService.java
    - Split parameter from 10 parameters to 3 object parameter ( Transaction.java, Bank.java , Reference.java) and 1 parameter (amount)
    - Split inquiry method
    - Refactor code
2. InquiryService.java
    - Refactor code
3. [BankProxyGateway.java](http://bankproxygateway.java) 
    - change parameter of requestTransfer method
4. [TransferResponse.java](http://transferresponse.java) and InquiryServiceResultDTO.java
    - use 'lombok' to refactor code
5. BankResponseCode.java
    - use enum class to keep static value (bank response code)

I'm not sure about InquiryServiceResultDTO reasons code and description in InquiryService and InquiryServiceTest. I see some part of code has the same description but different reasons code or same code but different description and try to change it. If I misunderstand. I'm so sorry about that.

---
# Exam


You are working for Software house company. You had assigned task for improving legacy service. The service for inquiry information from bank by bank account. 
The test codes had been done, so you can refactor, redesign and/or rewirte the code to support current test codes.


 - Class test: **InquiryServiceTest.java**

 - Class code: **InquiryService.java**


If you have any question, feel free to ask <sunpawet.som@ascendcorp.com>


## Instruction


1. Do refactor class **InquiryService.java**

2. Write unit test for testing all classes






## Rules

- Source code able to change, add new class, redesign

- You may use clean code technic and design pattern 

- Test must always pass

- New class should have test cover




## Data Dictionary for Bank Response Code


| Code        | Description           | |
| ------------- |-------------| 
| approved      | approved | 
| invalid_data      | 100:1091:Data type is invalid.      |  
| invalid_data | General error.     |
| transaction_error |      |
| transaction_error | Transaction error.     |
| transaction_error | 100:1091:Transaction is error with code 1091.    |
| transaction_error | 1092:Transaction is error with code 1092.    |
| transaction_error | 98:Transaction is error with code 98.    |
| unknown |    |
| unknown | 5001:Unknown error code 5001   |
| unknown | 5002:   |
| unknown | General Invalid Data code 501   |
| not_support |     Not Support |